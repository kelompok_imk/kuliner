<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login/signup.css">

    <!-- Fonts google -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:opsz,wght@6..12,400;6..12,600;6..12,700&display=swap" rel="stylesheet">
    
    <title>SignUp</title>
  </head>
  <body>
    
    <section class="login d-flex">
      <div class="login-left w-50 h-100">
        <img src="img/login/Rectangle 4.png" alt="">
      
          
        </div>

        <div class="login-right w-50 h-100">
          <div class="row">
            <div class="col-6">
            </div>
            
        
            </div>
            <div class="header">
              <h1>Segera Daftar!</h1>
              <p>Temukan rekomendasi tempat makanmu</p>
                
            <div class="login-form">
           
            <?php
              require_once "config.php";

              if (isset($_POST["submit"])) {
                  $name = $_POST["fullname"];
                  $username = $_POST["username"];
                  $email = $_POST["email"];
                  $birth = $_POST["tanggal_lahir"];
                  $password = $_POST["kata_sandi"];

                  $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                  $errors = array();

                  if (empty($name) || empty($password) || empty($username) || empty($birth)) {
                      array_push($errors, "All fields are required");
                  }
                  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                      array_push($errors, "Email is not valid");
                  }
                  if (strlen($password) < 8) {
                      array_push($errors, "Password must be at least 8 characters long");
                  }

                  if (count($errors) > 0) {
                      foreach ($errors as $error) {
                          echo "<div class='alert alert-danger'>$error</div>";
                      }
                  } else {
                      $sql = "INSERT INTO user_form(name, username, email, birth, password) VALUES (?, ?, ?, ?, ?)";
                      $stmt = mysqli_stmt_init($conn);

                      if ($stmt) {
                          if (mysqli_stmt_prepare($stmt, $sql)) {
                              mysqli_stmt_bind_param($stmt, "sssss", $name, $username, $email, $birth, $passwordHash);
                              mysqli_stmt_execute($stmt);
                              echo "<div class='alert alert-success'>You are registered successfully.</div>";
                              mysqli_stmt_close($stmt);
                          } else {
                              echo "<div class='alert alert-danger'>Error in preparing statement</div>";
                          }
                      } else {
                          echo "<div class='alert alert-danger'>Error in initializing statement</div>";
                      }
                  }
              }
              ?>
             

              <form action="register.php" class="form" method="post">
                  <label for="Nama Lengkap" class="form-label1">Nama Lengkap</label>
                  <input type="Nama Lengkap" class="form-control" name="fullname"id="Nama Lengkap" placeholder="nama lengkap ">
          
                  <label for="Nama Pengguna" class="form-label1">Nama Pengguna</label>
                  <input type="Nama Pengguna" class="form-control" name="username"id="Nama Lengkap" placeholder="@Suzie123">

                  <label for="Surel" class="form-label1">Surel</label>
                  <input type="Surel" class="form-control" name="email"id="Surel" placeholder="abc@mail.com">
                  
                  <label>Tanggal Lahir</label>
                  <input class="form-control" type="date" value="" name="tanggal_lahir" >

                  <label for="Kata Sandi" class="form-label">Kata Sandi</label>
                  <input type="password" class="form-control" name="kata_sandi"id="Kata Sandi" placeholder="kata sandi">
                  
                  
                 
                  
                  
                 
                
              <hr>
              </div>
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                  <label class="form-check-label" for="inlineCheckbox1">Setuju dengan syarat dan ketentuan</label>
                  
                  <div class="tombol">
                      <button type="submit" class="btn masuk" name="submit">Daftar</button>
                  </div>
                 
            </form>
            <div class="tombol">
                    <span class="d-inline">Sudah punya akun? <a href="login.php" class="login d-inline text-decoration-none">Masuk</a></span>
                </div>
            </div>
        
      </div>
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>