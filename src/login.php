<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login/login.css">

    <!-- Fonts google -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600;700&display=swap" rel="stylesheet">

    <title>Login</title>
</head>

<body>
    <section class="login d-flex">
        <div class="login-left w-50 h-100">
            <img src="img/login/4059670.jpg" alt="">
        </div>

        <div class="login-right w-50 h-100">
            <div class="header">
                <h1>Selamat Datang!</h1>
                <p>Temukan rekomendasi tempat makanmu</p>
                
                <button type="submit" class="btn login-google">
                    <img src="img/login/flat-color-icons_google.png" alt="">
                    Masuk Dengan Google
                </button>
            </div>

            <div class="tulisan">
                <p>------------- masuk dengan surel ------------- </p>
            </div>

            <div class="container">
                <form action="profil.html" class="form" method="post">
                    <?php
                    if (isset($_POST["submit"])) {
                        require_once "config.php";
                        $email = $_POST["email"];
                        $password = $_POST["kata_sandi"];
                        $sql = "SELECT * FROM user_form WHERE email='$email'";
                        $result = mysqli_query($conn, $sql);
                        $user = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        if ($user) {
                            if (password_verify($password, $user["password"])) {
                                header("Location:profil.php");
                                die();
                            } else {
                                echo "<div class='alert alert-danger'>Password does not match</div>";
                            }
                        } else {
                            echo "<div class='alert alert-danger'>Email does not match</div>";
                        }
                    }
                    ?>
                    <label for="Surel" class="form-label">Surel</label>
                    <input type="email" class="form-control" name="email" id="Surel" placeholder="abc@mail.com">

                    <label for="Kata Sandi" class="form-label">Kata Sandi</label>
                    <input type="password" class="form-control" name="kata_sandi" id="Kata Sandi" placeholder="kata sandi">

                      <div class="form-check d-flex justify-content-between">
                        <div>
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">Ingat saya</label>
                        </div>
                        <a href="forgot_password.php" class="d-inline text-decoration-none">Lupa kata sandi?</a>
                    </div>


                    <div class="tombol">
                        <button type="submit" class="btn masuk" name="submit">Masuk</button>
                    </div>
                </form>

                <div class="tombol">
                    <span class="d-inline">Belum punya akun? <a href="register.php" class="login d-inline text-decoration-none">Buat akun</a></span>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>
